﻿<#
.Synopsis
   Query for Storage Groups
.DESCRIPTION
   Long description
.EXAMPLE
   Example of how to use this cmdlet
.EXAMPLE
   Another example of how to use this cmdlet
#>

function Get-EMCStorageGroup
{
    [CmdletBinding()]
    [Alias()]
    [OutputType([int])]
    Param
    (
        # A Powershell based filter -NOT IMPLIMENTED YET-
        $Filter,

        # Param2 help description
        [string]
        $Host,

        [pscredential]
        $Credential
    )

    Begin
    {
        if(-not $host){$host = $script:host}
        if(-not $Credential){$credential = $script:Cred}
        
        ## this assume its best to only call the query once and filter locally, if its better to filter via navi, move to Process

        [xml]$SG = & $script:navi -Xml -h $Host -user $credential.UserName -password (getpassword $credential) storageGroup -list

        ## make it in to a useful object

        $result = foreach($group in $sg.whatever)
        {
            [pscustomobject]@{
                Name = $group.name
                HLU = foreach($hlu in $group.'  hlu    alu'){... split ./.. }
                }
            
        }
        
        ## define type of object
        
        $result | %{$_.pstypename.insert("EMC.StorageGroup",0);

    }
    Process
    {
        ## filter object
        ## output on the fly
    }
    End
    {
        ## probably not needed
    }
}