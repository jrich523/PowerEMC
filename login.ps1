﻿

<#
.Synopsis
   Use this to set the unit you plan to work with
.DESCRIPTION
   Long description
.EXAMPLE
   Example of how to use this cmdlet
.EXAMPLE
   Another example of how to use this cmdlet
#>
function Connect-EMCHost
{
    [CmdletBinding()]
    [Alias()]
    [OutputType([int])]
    Param
    (
        # Param1 help description
        [Parameter(Mandatory=$true,
                   ValueFromPipelineByPropertyName=$true,
                   Position=0)]
        $Host,

        # Param2 help description
        [Credential]
        $Credential
    )

    ## ping it

    ## try to login
    
    $script:Host = $Host
    $script:Credential = $Credential

    $script:loggedin = $true

    $return = & $script:Navi
}