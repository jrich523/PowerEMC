﻿

function GetPassword
{
    Param
    ($credential)

    if(-not $credential){$credential = $script:Credential}
    $credential.getnetworkcredential().password
}

<#
.Synopsis
   Short description
.DESCRIPTION
   Long description
.EXAMPLE
   Example of how to use this cmdlet
.EXAMPLE
   Another example of how to use this cmdlet
#>
function set-sg
{
    [CmdletBinding()]
    [Alias()]
    [OutputType([int])]
    Param
    (
        # Param1 help description
        [Parameter(Mandatory=$true,
                   ValueFromPipelineByProperty=$true,
                   Position=0)]
        [EMC.StorageGroup]
        $InputObject,

        # Param2 help description
        [int]
        $Param2
    )

    Begin
    {
    }
    Process
    {
    }
    End
    {
    }
}